# sicp

SICP exercises and other stuff\
https://sarabander.github.io/sicp/

# Recommended Scheme implementations

### MIT/GNU Scheme

https://www.gnu.org/software/mit-scheme/  
http://groups.csail.mit.edu/mac/projects/scheme/  
Made by MIT! This is what the book is supposed to be using.

### Racket

https://racket-lang.org  
Very popular, simple to setup and easy to use Scheme implementation. Has a `sicp` plugin for maximum compatibility with the book.

### CHICKEN Scheme

http://www.call-cc.org  
*A practical and portable scheme system*. Can be compiled to C and has many extensions called "eggs".

# Other LISP dialects

*Warning: you will likely run into compatibility issues if you use something other than Scheme.*

## Common Lisp

https://common-lisp.net/downloads  
*Includes Emacs, SBCL, Git, Quicklisp, all configured and ready to use.*

### Steel Bank Common Lisp

http://www.sbcl.org

### Clozure CL

https://ccl.clozure.com

## Emacs Lisp

https://www.gnu.org/software/emacs/

## Clojure

https://clojure.org