(define (accumulate op init seq)
  (if (null? seq) init (op (car seq) (accumulate op init (cdr seq)))))

(define (n-accumulate op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (n-accumulate op init (map cdr seqs)))))

(define matrix1 (list (list 1 2 3 4) (list 4 5 6 6) (list 6 7 8 9)))
(define matrix2 (list (list 7 6 5) (list 4 3 2) (list 8 3 1) (list 0 0 2)))
(define vector1 (list 1 2 3 4))
(define vector2 (list 2 -1 4 -3))


(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(dot-product vector1 vector2)


(define (matrix-*-vector m v)
  (map (lambda (x) (dot-product x v)) m))

(matrix-*-vector matrix1 vector1)


(define (transpose-matrix m)
  (n-accumulate cons '() m))

(transpose-matrix matrix1)


(define (matrix-*-matrix m1 m2)
  (let ((cols (transpose-matrix m2)))
    (map (lambda (x) (matrix-*-vector cols x)) m1)))

(matrix-*-matrix matrix1 matrix2)
