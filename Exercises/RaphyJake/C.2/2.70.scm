(define (make-leaf symbol weight)
  (list 'leaf symbol weight))

(define (leaf? obj) (eq? (car obj) 'leaf))

(define (symbol-leaf leaf) (cadr leaf))
(define (weight-leaf leaf) (caddr leaf))

(define (make-code-tree left right)
  (list left
        right
        (append (symbol left) (symbol right))
        (+ (weight left) (weight right))))

(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))

(define (symbol tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (decode bits tree)
  (define (choose-branch bit branch)
    (cond ((= bit 0) (left-branch branch))
          ((= bit 1) (right-branch branch))
          (else #f)))

(define (decode-1 bits current-branch)
  (if (null? bits)
      '()
      (let ((next-branch (choose-branch (car bits) current-branch)))
        (if (leaf? next-branch)
            (cons (symbol-leaf next-branch)
                  (decode-1 (cdr bits) tree))
            (decode-1 (cdr bits) next-branch)))))

  (decode-1 bits tree))

(define (adjoint-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoint-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs) '()
      (let ((pair (car pairs)))
        (adjoint-set (make-leaf (car pair) (cadr pair))
                     (make-leaf-set (cdr pairs))))))

(define (encode message tree)

  (define (element-of-set? x set)
    (cond ((null? set) #f)
          ((eq? (car set) x) #t)
          (else (element-of-set? x (cdr set)))))

  (define (encode-symbol symb branch)
    (cond ((leaf? branch) '())
          ((element-of-set? symb (symbol (right-branch branch))) (cons 1
                                                                       (encode-symbol symb (right-branch branch))) )
          (else (cons 0
                      (encode-symbol symb (left-branch branch))))))


  (if (null? message)
      '()
      (append (encode-symbol (car message) tree) (encode (cdr message) tree))))



(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (successive-merge pairs)
  (if (null? (cdr pairs))
      (car pairs)
      (successive-merge (adjoint-set (make-code-tree (car pairs) (cadr pairs)) (cddr pairs)))))



(define rock-tree ( generate-huffman-tree `((A 2) (BOOM 1) (GET 2) (JOB 2) (NA 16) (SHA 3) (YIP 9) (WAH 1))))

(define song '(GET A JOB SHA NA NA NA NA NA NA NA NA NA GET A JOB SHA NA NA NA NA NA NA NA WAH YIP YIP YIP YIP YIP YIP YIP YIP TIP SHA BOOM))

(define encoded-song (encode song rock-tree))


(define (lenght x) (if (null? x) 0 (+ 1 (lenght (cdr x)))))


encoded-song
(lenght encoded-song)

(decode encoded-song rock-tree)


