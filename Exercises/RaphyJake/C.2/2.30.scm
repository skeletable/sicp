(define tree1 (list 1 (list 2 (list (list 3 4) ) 5) (list 6 7)))

(define (square-tree tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (* tree tree))
        (else (cons (square-tree (car tree)) (square-tree (cdr tree))))))

(define (square-tree1 tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (square-tree sub-tree)
             (* sub-tree sub-tree)
          )) tree))

((lambda (x) x) tree1)
(square-tree tree1)
(square-tree1 tree1)
