(define list1 (list 1 3 (list 5 7) 9))

(car (cdr (car (cdr (cdr list1)))))

; or..

(car (cdaddr list1))


(define list2 (list (list 7))) ; ??????????????

(car (car list2))

; ooor..

(caar list2)


(define list3 (list 1 2 3 4 5 6 7))

(car (cdr (cdr (cdr (cdr (cdr (cdr list3)))))))

; oor..

(cadddr (cdddr list3))
