
(define (reverse x)
  (define (reverse-iter a b)
    (if (null? a)
        b
        (reverse-iter (cdr a) (cons (car a) b))))
  (reverse-iter x (list)))

(display (reverse (list 1 2 3)))
