(define (foldr f init seq)
  (if (null? seq) init (f (car seq) (foldr f init (cdr seq)))))

(define (foldl f init seq)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (f result (car rest))
              (cdr rest))))
  (iter init seq))


(define (reverse-r seq)
  (foldr (lambda (x y) (append y (list x))) '() seq))

(define (reverse-l seq)
  (foldl (lambda (x y) (cons y x)) '() seq))

(define list1 (list 1 2 3 4))

(reverse-r list1)
(reverse-l list1)
