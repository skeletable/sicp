(define tree1 (list 1 2 (list 4 3 (list 3 4)) (list 3 4) 9))

(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq) (accumulate op init (cdr seq)))))


(define (count-leaves t)
  (accumulate (lambda (x y)
                (+ y x))
              0
              (map (lambda (x) (if (pair? x) (count-leaves x) 1)) t)))


(count-leaves tree1)
