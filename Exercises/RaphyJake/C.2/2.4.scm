(define (cons x y)
  (lambda (m) (x y)))

(define (car z)
  (z (lambda (p q) (p))))

(define (cdr z)
  (z (lambda (p q) (p))))
