(define x (list (list 1 2) (list 3 4)))

(define (fringe x)
  (if (null? x) '()
      (if (pair? x)
          (append (fringe (car x)) (fringe (cdr x)))
          (list x))))

(fringe x)

