

(define list2 (list (list 1 2 3) 3 (list 5 6 4)))

(define (deep-reverse x)
  (define (iter-reverse x a)
    (if (null? x)
        a
        (iter-reverse (cdr x) (cons (deep-reverse (car x)) a))))
  (if (pair? x)
      (iter-reverse x '())
       x))


(deep-reverse list2)
