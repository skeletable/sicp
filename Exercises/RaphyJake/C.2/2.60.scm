;; Nothing changes.
(define (element-of-set? x set)
  (cond ((null? set) #f)
        ((eq? x (car set)) #t)
        (else (element-of-set? x (cdr set)))))

;; Allowing duplicates.
(define (adjoint-set x set)
      (cons x set))

;;Again, nothing changes.
(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

;;Easy breezy.
(define union-set append)


(element-of-set? 'b (list 'a 'b 'c))

(intersection-set '(a b c 3) '(1 2 a c))


(union-set '(a b c 3) '(1 2 a c))


;; Insertion and union is faster.
