(define tree1 (list 1 (list 2 (list (list 3 4) ) 5) (list 6 7)))


(define (map-tree f tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (map-tree f sub-tree)
             (f sub-tree)
          )) tree))

(define (square-tree tree) (map-tree (lambda (x) (* x x)) tree))

((lambda (x) x) tree1)
(square-tree tree1)

