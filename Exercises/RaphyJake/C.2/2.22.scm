(define (map f xs)
  (if (null? xs)
      '()
      (cons (f (car xs)) (map f (cdr xs)))))

(define (square-list1 xs)
  (define (square x) (* x x))
  (if (null? xs)
      '()
      (cons (square (car xs)) (square-list1 (cdr xs)) )))

(define (square-list2 xs)
  (define (square x) (* x x))
  (map square xs))

(square-list1 (list 1 2 3 4 5))
(square-list2 (list 1 2 3 4 5))
