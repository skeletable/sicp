(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq) (accumulate op init (cdr seq)))))


(define (horner-eval x coeff-seq)
  (accumulate (lambda (n-coeff higher-terms)
                (+ n-coeff (* higher-terms)))
              0
              coeff-seq))

;; 1 + 3x + 5x^3 + x^5

(define poly (list 1 3 0 5 0 1))

(horner-eval 2 poly)
