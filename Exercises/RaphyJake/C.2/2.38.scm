(define (foldr f init seq)
  (if (null? seq) init (f (car seq) (foldr f init (cdr seq)))))

(define (foldl f init seq)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (f result (car rest))
              (cdr rest))))
  (iter init seq))


(foldr / 1 (list 1 2 3 4))
(foldl / 1 (list 1 2 3 4))
(foldr list '() (list 1 2 3 4))
(foldl list '() (list 1 2 3 4))

;; foldl and foldr are equal only if the operator is associative.

(foldl + 0 (list 1 2 3 4))
(foldr + 0 (list 1 2 3 4))
(foldl * 1 (list 1 2 3 4))
(foldr * 1 (list 1 2 3 4))
