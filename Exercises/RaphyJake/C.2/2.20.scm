(define (same-parity x . y)
  (define parity (mod x 2))
  (define (parity-filter k)
    (if (null? k)
         '()
         (if (= (mod (car k) 2) parity)
             (cons (car k) (parity-filter (cdr k)))
             (parity-filter (cdr k)))))
  (cons x (parity-filter y)))

(same-parity 1 2 3 4 5)

(same-parity 2 3 4 5 6 7 89)
