(define (accumulate op init seq)
  (if (null? seq) init (op (car seq) (accumulate op init (cdr seq)))))

(define (n-accumulate op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (n-accumulate op init (map cdr seqs)))))

(define list1 (list (list 1 2 3) (list 4 5 6) (list 7 8 9) (list 10 11 12)))

(n-accumulate + 0 list1)
