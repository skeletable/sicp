(define (for-each f xs)
  (define (discard x y) y)
  (if (null? xs)
      #t
      (for-each f (discard (f (car xs)) (cdr xs)))))

;; This only works with eager evaluation.

(for-each (lambda (x) (newline) (display x))
          (list 1 2 3 4 5))
