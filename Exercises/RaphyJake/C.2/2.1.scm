
(define (lowest-term p q)
  (let ((g (gcd p q)))
    (cons (/ p g) (/ q g))))

(define (make-rat n d)
  (let ((p (if (> (* n d) 0) (abs n) n))
        (q (abs d)))
        (lowest-term p q)))

(define numer car)
(define denom cdr)

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

(print-rat (make-rat -4 -5))
