
(define (make-point x y)
  (cons x y))

(define x-point car)
(define y-point cdr)

(define (make-segment p q)
  (cons p q))

(define start-segment car)
(define end-segment cdr)

(define (midpoint-segment a)
  (let ((average (lambda (x y) (/ (+ x y) 2))))
    (make-point (average (x-point (start-segment a))
                         (x-point (end-segment a)))
                (average (y-point (start-segment a))
                         (y-point (end-segment a))))))


(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(print-point (midpoint-segment (make-segment (make-point 10 30) (make-point -1 -1))))
