(define (element-of-set? x set)
  (cond ((null? set) #f)
        ((eq? x (car set)) #t)
        (else (element-of-set? x (cdr set)))))

(define (adjoint-set x set)
  (if (element-of-set? x set)
      set
      (cons x set)))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(element-of-set? 'b (list 'a 'b 'c))

(intersection-set '(a b c 3) '(1 2 a c))

(define (accumulate op init set)
  (if (null? set)
      init
      (op (car set) (accumulate op init (cdr set)))))

(define (union-set set1 set2)
  (accumulate adjoint-set set1 set2))


(union-set '(a b c 3) '(1 2 a c))
