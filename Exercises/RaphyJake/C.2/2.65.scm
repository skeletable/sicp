(define entry car)
(define left-branch cadr)
(define right-branch caddr)

(define (make-tree entry left right) (list entry left right))


(define (tree->list tree)
  (define (copy-to-list tree result)
    (if (null? tree)
        result
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list (right-branch tree) result)))))
  (copy-to-list tree '()))



(define (list->tree elements)
  (car (partial-tree elements (length elements))))


(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result (partial-tree
                                 (cdr non-left-elts)
                                 right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts (cdr right-result)))
                (cons (make-tree
                       this-entry left-tree right-tree)
                      remaining-elts))))))))

(define (element-of-set x set)
  (cond ((null? set) #f)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set x (cdr set)))))

(define (adjoint-set x set)
  (cond ((null? set) (list x))
        ((= x (car set)) set)
        ((< x (car set)) (cons x set))
        (else (cons (car set) (adjoint-set x (cdr set))))))

(define (intersection-set s1 s2)
  (if (or (null? s1) (null? s2))
      '()
      (let ((x1 (car s1)) (x2 (car s2)))
        (cond ((= x1 x2)
               (cons x1 (intersection-set (cdr s1) (cdr s2))))
              ((< x1 x2) (intersection-set (cdr s1) s2))
              (else (intersection-set (cdr s2) s1))))))

(define (accumulate op init list)
  (if (null? list) init (op (car list) (accumulate op init (cdr list)))))

(accumulate adjoint-set '() (list 1 2 3 7 8 62 2 1))

(define (union-set s1 s2)
  (cond ((null? s1) s2)
        ((null? s2) s1)
        (else (let ((x1 (car s1))
                    (x2 (car s2))
                    (rem1 (cdr s1))
                    (rem2 (cdr s2)))
              (cond ((= x1 x2) (cons x1 (union-set rem1 rem2)))
                    ((< x1 x2) (cons x1 (union-set rem1 s2)))
                    (else (cons x2 (union-set s1 rem2))))))))



;; THis is freakin' dumb


(define (intersection-tree tree1 tree2)
  (let ((s1 tree->list tree1)
        (s2 tree->list tree2))
    (list->tree (intersection-set s1 s2))))

(define (union-tree tree1 tree2)
  (let ((s1 tree->list tree1)
        (s2 tree->list tree2))
    (list->tree (union-set s1 s2))))



