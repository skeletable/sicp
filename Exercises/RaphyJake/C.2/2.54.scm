(define (equal2? x1 x2)
  (if (or (null? x1) (null? x2))
      #t
      (and (eq? (car x1) (car x2))
           (equal2? (cdr x1) (cdr x2)))))

(define list1 (list 1 2 3 4 5))

(define list2 (list 1 2 3 4 5))

(equal1? list1 list2)
(equal2? list1 list2)
