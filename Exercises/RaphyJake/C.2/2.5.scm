
(define (check2 x)
  (define (it-check x n)
    (if (= (remainder x 2) 0)
        (it-check (/ x 2) (+ 1 n))
        n)
    )
  (it-check x 0))

(define (check3 x)
  (define (it-check x n)
    (if (= (remainder x 3) 0)
        (it-check (/ x 3) (+ 1 n))
        n)
    )
  (it-check x 0))


(define (cons x y)
  (lambda (m) (m (expt 2 x) (expt 3 y))))


(define (car z)
  (z (lambda (x y) (check2 x))))

(define (cdr z)
  (z (lambda (x y) (check3 y))))



(car (cons 2 3))

(cdr (cons 2 3))
