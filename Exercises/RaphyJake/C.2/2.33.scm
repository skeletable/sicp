(define (accumulate operator initial values)
        (if (null? values)
            initial
            (operator (car values)
                      (accumulate operator initial (cdr values)))))

(define list1 (list 1 2 3))
(define list2 (list 3 4 5))

(define (map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) '() sequence))


(define (append seq1 seq2)
  (accumulate cons seq2 seq1))

(define (lenght sequence)
  (accumulate (lambda (x y)
                (+ y (if (null? x) 0 1))) 0 sequence))

(append list1 list2)

(lenght list1)

(map (lambda (x) (* x x)) list1)
