(define (element-of-set x set)
  (cond ((null? set) #f)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set x (cdr set)))))

(define (adjoint-set x set)
  (cond ((null? set) (list x))
        ((= x (car set)) set)
        ((< x (car set)) (cons x set))
        (else (cons (car set) (adjoint-set x (cdr set))))))

(define (intersection-set s1 s2)
  (if (or (null? s1) (null? s2))
      '()
      (let ((x1 (car s1)) (x2 (car s2)))
        (cond ((= x1 x2)
               (cons x1 (intersection-set (cdr s1) (cdr s2))))
              ((< x1 x2) (intersection-set (cdr s1) s2))
              (else (intersection-set (cdr s2) s1))))))

(define (accumulate op init list)
  (if (null? list) init (op (car list) (accumulate op init (cdr list)))))

(accumulate adjoint-set '() (list 1 2 3 7 8 62 2 1))

(adjoint-set 6 '(1 2 3 4))
