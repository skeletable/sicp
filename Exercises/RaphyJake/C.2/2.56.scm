(define (variable? x) (symbol? x))

(define (=number? exp num)
  (and (number? exp) (= exp num)))

(define (same-variable? x y )
  (and (variable? x) (variable? y) (eq? x y)))



(define (make-sum a b)
  (cond ((=number? a 0) b)
        ((=number? b 0) a)
        ((and (number? a) (number? b)) (+ a b))
        (else (list '+ a b))))

(define (sum? exp) (and (pair? exp) (eq? '+ (car exp))))

(define (addend s) (cadr s))
(define (augend s) (caddr s))



(define (make-product a b)
  (cond ((or (=number? a 0) (=number? b 0)) 0)
        ((=number? b 1) a)
        ((=number? a 1) b)
        ((and (number? a) (number? b)) (* a b))
        (else (list '* a b))))

(define (product? exp) (and (pair? exp) (eq? '* (car exp))))

(define (multiplier s) (cadr s))
(define (multiplicand s) (caddr s))



(define (make-exponent base exponent)
  (cond ((=number? exponent 1) base)
        ((=number? exponent 0) 1)
        (else (list '** base exponent))))

(define (exponent? exp) (and (pair? exp) (eq? '** (car exp))))

(define (base exp) (cadr exp))
(define (exponent exp) (caddr exp))



(define (derive exp variable)
  (cond ((number? exp) 0)
        ((variable? exp)
         (if (same-variable? exp variable) 1 0))
        ((sum? exp) (make-sum
                     (derive (addend exp) variable)
                     (derive (augend exp) variable)))
        ((product? exp) (make-sum
                         (make-product (multiplier exp)
                                       (derive (multiplicand exp) variable))
                         (make-product (multiplicand exp)
                                       (derive (multiplier exp) variable))))
        ((exponent? exp) (make-product (exponent exp)
                                       (make-exponent (base exp) (- (exponent exp) 1))))
        (else #f)))

(derive '(+ x (* x 6)) 'x)
(derive 'x 'x)
(derive 7 'x)
(derive '(* 3 (* y x)) 'x)
(derive '(* 7 (+ (** x 3) 9)) 'x) ;; 7*(x^3 + 9)


;; There is room for improvement.
