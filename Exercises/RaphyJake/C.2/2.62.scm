(define (element-of-set x set)
  (cond ((null? set) #f)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set x (cdr set)))))

(define (adjoint-set x set)
  (cond ((null? set) (list x))
        ((= x (car set)) set)
        ((< x (car set)) (cons x set))
        (else (cons (car set) (adjoint-set x (cdr set))))))

(define (intersection-set s1 s2)
  (if (or (null? s1) (null? s2))
      '()
      (let ((x1 (car s1)) (x2 (car s2)))
        (cond ((= x1 x2)
               (cons x1 (intersection-set (cdr s1) (cdr s2))))
              ((< x1 x2) (intersection-set (cdr s1) s2))
              (else (intersection-set (cdr s2) s1))))))

(define (accumulate op init list)
  (if (null? list) init (op (car list) (accumulate op init (cdr list)))))

(accumulate adjoint-set '() (list 1 2 3 7 8 62 2 1))

(define (union-set s1 s2)
  (cond ((null? s1) s2)
        ((null? s2) s1)
        (else (let ((x1 (car s1))
                    (x2 (car s2))
                    (rem1 (cdr s1))
                    (rem2 (cdr s2)))
              (cond ((= x1 x2) (cons x1 (union-set rem1 rem2)))
                    ((< x1 x2) (cons x1 (union-set rem1 s2)))
                    (else (cons x2 (union-set s1 rem2))))))))

(union-set '(1 3 5 6 7 10 13) '(1 2 3 5 6 8))

;; Yay.
