
(define (last-pair x)
  (if (null? (cdr x))
      (car x)
      (last-pair (cdr x))))

(display (last-pair (list 1 2 3)))
