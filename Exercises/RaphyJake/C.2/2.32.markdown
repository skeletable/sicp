```lisp
(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (x) (cons (car s) x)) rest)))))
```


To find all the subsets of a set, (x [..])
We cons the `car` of the set to every subset of the `cdr`, and append this list to the list of subsets of its `cdr`.

Example:

`(subsets (list 2 3))`

The subsets of the cdr of this list are: `((3) ())`.   (1)
We now add `2`, the car of the list, to this list of subsets : `((2 3) (2))` (2)
And then we append (1) to (2): `((2 3) (2) (3) ())`
