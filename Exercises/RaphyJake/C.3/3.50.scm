(define (stream-map proc . argstreams)
  (if (null? (car argstreams))
      '()
      (cons-stream (apply proc (map stream-car argstream))
                   (apply stream-map
                          (cons proc (map stream-cdr argstream))))))
