(define v (list 'a 'b 'c 'd))

(define (mistery x)
  (define (loop x y)
    (if (null? x)
        y
        (let ((temp (cdr x)))
          (set-cdr! x y)
          (loop temp x))))

  (loop x '()))

(define w (mistery v))

w
