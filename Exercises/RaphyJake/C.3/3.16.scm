(define (count-pairs x)
  (if (not (pair? x))
      0
      (+ (count-pairs (car x))
         (count-pairs (cdr x))
         1)))

(define temp (cons '() '()))


(define a (list 1 2 3))

(define b (cons temp (cons temp '())))


(define temp2 (cons temp temp))

(define c (cons temp2 temp2))

(count-pairs a)
(count-pairs b)
(count-pairs c)
