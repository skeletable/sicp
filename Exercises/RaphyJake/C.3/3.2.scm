(define (monitor-function f)
  (define counter 0)
  (lambda (x) (if
               (eq? x 'how-many-calls?)
               counter
               (begin
                 (set! counter (+ counter 1))
                 (f x)))))

(define s (monitor-function sqrt))

(s 25)
(s 144)

(s 'how-many-calls?)
