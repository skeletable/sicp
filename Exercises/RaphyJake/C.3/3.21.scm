(define (front-ptr x) (car x))
(define (rear-ptr x) (cdr x))


(define (set-front-ptr! queue item) (set-car! queue item))
(define (set-rear-ptr! queue item) (set-cdr! queue item))


(define (empty-queue? queue) (null? (front-ptr queue)))

(define (make-queue) (cons '() '()))


(define (front-queue queue)
  (if (empty-queue? queue)
      "AAAAAAAAAAAA"
      (car (front-ptr queue))))

(define (insert-queue! queue item)
  (let ((new-pair (cons item '())))
    (cond ((empty-queue? queue) (set-front-ptr! queue new-pair) (set-rear-ptr! queue new-pair) queue)
          (else (set-cdr! (rear-ptr queue) new-pair) (set-rear-ptr! queue new-pair) queue))))

(define (delete-queue! queue)
  (cond ((empty-queue? queue) "AAAAAAAAAAAAAAAAA")
        (else (set-front-ptr! queue (cdr (front-ptr queue))) queue)))


;;Here comes Ben
(define q1 (make-queue))

(insert-queue! q1 'a) 

(insert-queue! q1 'b)

(delete-queue! q1)

(delete-queue! q1)


;; Lisp will try to print the queue as it is: A pair with a list as first element and another pair as second element.

;;The first element is the actual queue. The second element of the pair is just the rear-ptr.
;;Which, by the way, does not get deleted when we delete-queue!.

(define (pretty-print queue) (display (car queue)) (newline))

(define q2 (make-queue))

(pretty-print (insert-queue! q2 'a))

(pretty-print (insert-queue! q2 'b))

(pretty-print (delete-queue! q2))

(pretty-print (delete-queue! q2))
