Using Louis' code, when we expose the serializer while ALSO serializing internally all our procedures, we might run into a double-serialization layer:

The user is trying to serialize "deposit" and "withdraw", but they're already serialized internally. These would never run.
