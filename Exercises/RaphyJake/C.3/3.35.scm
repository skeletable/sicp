(define (adder a1 a2 sum)
  (define (process-new-value)
    (cond ((and (has-value? a1) (has-value? a2))
           (set-value! sum
                       (+ (get-value a1)
                          (get-value a2)) me))
          ((and (has-value? a1) (has-value? sum))
           (set-value! a2
                       (- (get-value sum)
                          (get-value a1)) me))

          ((and (has-value? a2) (has-value? sum))
           (set-value! a1
                       (- (get-value sum)
                          (get-value a2)) me))))

  (define (process-forget-value)
    (forget-value! sum me)
    (forget-value! a1 me)
    (forget-value! a2 me)
    (process-new-value))

  (define (me request)
    (cond ((eq? request 'I-have-a-new-value) (process-new-value))
          ((eq? request 'I-lost-my-value) (process-forget-value))
          (else "AAAAAAAAAAAAAAAAAAAAA")))
  (connect a1 me)
  (connect a2 me)
  (connect sum me)
  me)

(define (multiplier a1 a2 product)
  (define (process-new-value)
    (cond
     ((or (and (has-value? a1) (= 0 (get-value a1)))
          (and (has-value? a2) (= 0 (get-value a2)))) (set-value! product 0 me))
     ((and (has-value? a1) (has-value? a2)) (set-value! product (* (get-value a1)
                                                                   (get-value a2)) me))
     ((and (has-value? a1) (has-value? product))
      (set-value! a2
                  (/ (get-value product)
                     (get-value a1)) me))

     ((and (has-value? a2) (has-value? product))
      (set-value! a1
                  (/ (get-value product)
                     (get-value a2)) me))))

  (define (process-forget-value)
    (forget-value! product me)
    (forget-value! a1 me)
    (forget-value! a2 me)
    (process-new-value))

  (define (me request)
    (cond ((eq? request 'I-have-a-new-value) (process-new-value))
          ((eq? request 'I-lost-my-value) (process-forget-value))
          (else (display "AAAAAAAAAAAAAAAAAAAAA"))))
  (connect a1 me)
  (connect a2 me)
  (connect product me)
  me)

(define (squarer a b)
  (define (square x) (* x x)) ; duh

  (define (process-new-value)
    (cond ((has-value? a) (set-value! b (square (get-value a)) me))
          ((has-value? b) (if (> 0 (get-value b))
                              (display "SHIT!! FUCK!!! ERROR")
                              (set-value! a (sqrt (get-value b)) me)
                              ))))
  (define (process-forget-value)
    (forget-value! a me)
    (forget-value! b me))
  (define (me request)
    (cond ((eq? request 'I-have-a-new-value) (process-new-value))
          ((eq? request 'I-lost-my-value) (process-forget-value))
          (else (display "AAAAAAAAAAAAAAAAAAAAA"))))
  (connect a me)
  (connect b me)
  me)


(define (averager a b c)
  (let ((u (make-connector))
        (v (make-connector)))
    (constant 2 u)
    (adder a b v)
    (multiplier u c v)))


(define (inform-about-value constraint) (constraint 'I-have-a-new-value))
(define (inform-about-no-value constraint) (constraint 'I-lost-my-value))


(define (constant value connector)
  (define (me request) (display "FUUUUUUUUUUUCK!!!!!!"))
  (connect connector me)
  (set-value! connector value me)
  me)

(define (probe name connector)
  (define (print-probe value)
    (newline)
    (display "Probe: " )
    (display name)
    (display " = ")
    (display value))
  (define (process-new-value) (print-probe (get-value connector)))
  (define (process-forget-value) (print-probe "?"))
  (define (me request)
    (cond ((eq? request 'I-have-a-new-value) (process-new-value))
          ((eq? request 'I-lost-my-value) (process-forget-value))
          (else (display "FUUUUUUUUUUUCK!"))))
  (connect connector me)
  me)


(define (make-connector)
  (let ((value #f) (informant #f) (constraints '()))

       (define (set-my-value newval setter)
         (cond ((not (has-value? me)) (set! value newval) (set! informant setter)
                (for-each-except setter inform-about-value constraints))
               ((not (= value newval)) (display "SHIT!"))
               (else 'ignored)))

       (define (forget-my-value retractor) 
         (if (eq? retractor informant)
             (begin (set! informant #f) (set! value #f) (for-each-except retractor inform-about-no-value constraints))
             'ignored))
       (define (connect new-constraint)
         (if (not (memq new-constraint constraints))
             (set! constraints (cons new-constraint constraints)) 'a)
         (if (has-value? me) (inform-about-value new-constraint) 'a)
         'done)
       (define (me request)
         (cond ((eq? request 'has-value?) (if informant #t #f))
               ((eq? request 'value) value)
               ((eq? request 'forget) forget-my-value)
               ((eq? request 'set-value!) set-my-value)
               ((eq? request 'connect) connect)
               (else (display "SHIT!!!!!!!!!!!!!!"))))
       me))

(define (for-each-except exception procedure list)
  (define (loop items)
    (cond ((null? items) (newline) 'done)
          ((eq? (car items) exception) (loop (cdr items)))
          (else (procedure (car items)) (loop (cdr items)))))
  (loop list))

(define (has-value? connector) (connector 'has-value?))

(define (get-value connector) (connector 'value))

(define (set-value! connector new-value informant) ((connector 'set-value!) new-value informant))

(define (forget-value! connector retractor) ((connector 'forget) retractor))

(define (connect connector new-constraint) ((connector 'connect) new-constraint))






(define B (make-connector))
(define A (make-connector))


(probe "A" A)
(probe "B" B)

(squarer A B)

(set-value! A 10 'user)
(forget-value! A 'user)

(set-value! B 256 'user)
(set-value! A 10 'user)
