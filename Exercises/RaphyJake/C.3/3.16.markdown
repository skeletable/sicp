What is wrong with you, Ben?


This returns 3 as Ben intended.

```

[ ][ ]->[ ][ ]->[ ][/]
 |       |       |
[1]     [2]     [3]
```

This is just `(list 1 2 3)`, nothing fancy.


This returns 4 pairs.

```
[ ][ ]
 |  |
 |  |
 | [ ][/]
 |  |
 |  |
 v  v
[/][/]
```

This is `(define a (cons '() '()))` and then `(cons a (cons a '()))`


THis returns 7 pairs.


```
[ ][ ]
 |  |
 v  v
[ ][ ]
 |  |
 v  v
[/][/]
```


You can conjure such being this way:


```
(define temp (cons '() '()))
(define temp2 (cons temp temp)
(define c (cons temp2 temp2))
```
No mutation needed!


This runs forever.

```
 |---|
 |   |
 |  [ ][ ]->[ ][ ]->[ ][/]
 |   ^
 |---|
```

Just do `(define a (list '() '() '())` and `(set-car! a a)`


I think you can create this like ()
