(define (monte-carlo trials experiment)
  (define (iter trials-remaining trials-passed)
    (cond ((= trials-remaining 0) (/ trials-passed trials))
           ((experiment) (iter (- trials-remaining 1) (+ trials-passed 1)))
           (else (iter (- trials-remaining 1) trials-passed))))
  (iter trials 0))

(define (estimate-integral p x1 x2 y1 y2)
  (define (randomrange low high)
    (let ((range (- high low))) (+ low (random range))))

  (define (randpredicate p)
    (lambda () (p (randomrange x1 x2) (randomrange y1 y2))))

  (* (monte-carlo 1000000 (randpredicate p)) (* (- x1 x2) (- y1 y2))))

(define (circle x y) (< (+ (* x x) (* y y)) 1))

(define pi (estimate-integral circle -1.0 1.0 -1.0 1.0))

pi


