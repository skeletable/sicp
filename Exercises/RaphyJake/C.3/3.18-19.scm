
;;Good ol turtle and hare!

(define (check-cycle xs)

  (define (rec fast slow)
    (cond ((null? fast) #f)
          ((null? (cdr fast)) #f)
          ((eq? fast slow) #t)
          (else (rec (cddr fast) (cdr slow)))))

  (if (null? xs)
      #f
      (rec (cdr xs) xs)))

(define a (list 1 2 3 4))

(define c (list 1 2 3 4 5 6 7))

(set-cdr! (cddddr c) c)



(check-cycle a)
(check-cycle c)
