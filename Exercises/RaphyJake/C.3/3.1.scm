(define (make-accumulator amount)
  (lambda (n) (begin (set! amount (+ amount n)) amount)))

(define A (make-accumulator 10))
(define B (make-accumulator 100))

(A 10)
(B 10)

(A 20)
(B 30)
