(define (mul-stream x y) (stream-map * x y))

(define factorials (cons-stream 1 (mul-stream (cdr integers) factorial)))
