```
(define x 10)

(define s (make-serializer))

(parallel-execute (lambda () (set! x (* x x))) 
                  (lambda () (set! x (* x x x))))

```

Let's call these P1 and P2.

1000000: P1, and after that P2. No interleaving.
1000000: P2, and after that P1, No interleaving.

100000: P1 finds x = 10 and x= 10. P2 finds x = 10. P1 sets x:=100. P2 find x=100, x = 100. P2 sets x:= 100000

10000: P1 finds x=10 and x=10. P2 finds x = 10 and x = 10. P1 sets x:=100. P2 finds x=100. P2 sets x:= 10000

1000 P1 finds x=10 and x=10. P2 finds x=10, x=10 and x=10. P1 sets x:=100. P2 sets x:= 1000.

10000: P2 finds x=10, x=10, x=10. P1 finds x=10. P2 sets x:=1000. P2 finds x=1000. P1 sets x:=10000

P2 finds x=10, x=10, x=10. P1 finds x=10, x=10. P2 sets x:= 1000. P1 sets x:= 100.


After serializing, only the first two cases remain valid.
