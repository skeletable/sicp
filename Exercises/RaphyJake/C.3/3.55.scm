(define (partial-sum stream) (stream-cons 0 (add-stream (partial-sum stream) stream)))
