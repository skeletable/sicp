(define (make-account balance password)
  (define fbi-open-up 7)

  (define call-the-cops "WEEEWOOOOWEEWOOO")

  (define (password-protect f)
    (lambda (x s) (if (eq? s password)
                      (f x)
                      (begin (set! fbi-open-up (- fbi-open-up 1))
                             (cond ((= fbi-open-up 0) call-the-cops)
                                    ((= fbi-open-up 1) "LAST WARNING!!!")
                                    (else "ERROR! MORON"))))))

  (define (withdraw amount)
    (if (>= balance amount)
        (begin (set! balance (- balance amount)) balance)
        "ERROr, you are poor!"))

  (define (deposit amount)
    (begin (set! balance (+ balance amount)) balance))

  (define (dispatch m)
    (cond ((eq? m 'withdraw) (password-protect withdraw))
          ((eq? m 'deposit) (password-protect deposit))
          (else "ERROR!! What fuck?")))
  dispatch)

(define bob (make-account 0 'bonkers))

((bob 'deposit) 1000 'bonkers)

;;A thief approaches.
((bob 'withdraw) 10 'alice-is-sexy)
((bob 'withdraw) 100 'bob1234)
((bob 'withdraw) 99 'come-on)
((bob 'withdraw) 1 'yourmom)
((bob 'withdraw) 2 'bob4ever123)
((bob 'withdraw) 53 'bobbobbob3)
((bob 'withdraw) 23 'im-gettin-pissed)
