

 (define (display-stream-until n s)     ; n-th value included

     (if (< n 0)

         the-empty-stream

         (begin (newline) (display (stream-car s))

                (display-stream-until (- n 1) (stream-cdr s)))))

 

; It can be easily seen that (mul-series (stream-cdr s1) s2) equals

                                        ; (add-streams (scale-stream (stream-cdr s2) (stream-car s1)) 

 ;              (cons-stream 0 (mul-series (stream-cdr s1) (stream-cdr s2)))) 

 ; , which means that meteorgan's solution and atupal's solution are equivalent. 

 (define (mul-series s1 s2) 

     (cons-stream (* (stream-car s1) (stream-car s2)) 

                  (add-streams (scale-stream (stream-cdr s1) (stream-car s2)) 

                               (mul-series (stream-cdr s2) s1)))) 

  

 (define circle-series 

     (add-streams (mul-series cosine-series cosine-series) 

                  (mul-series sine-series sine-series))) 

  

 ; if you see one 1 followed by 0's, your mul-series is correct. 

 (display-stream-until 30 circle-series) 

  
