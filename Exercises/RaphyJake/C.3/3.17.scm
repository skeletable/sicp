(define (count-pairs x)
  (define (is-in-set? x set)
    (if (null? set) #f
        (if (eq? x (car set))
            #t
            (is-in-set? x (cdr set)))))

  (define (add-everywhere x xs)
    (if (null? xs)
        (set! xs (list x))
        (let ((temp (cdr xs))) (set-cdr! xs (cons x temp)))) xs)

  (define (lenght xs)
    (if (null? xs)
        0
        (+ 1 (lenght (cdr xs)))))

  (define (rec x pairs)
    (if (not (pair? x))
        0
        (if (not (is-in-set? x pairs))
            (let ((xs (add-everywhere x pairs)))
                   (rec (car x) xs)
                   (rec (cdr x) xs)
                   (lenght xs))
            0)))

  (rec x '()))

(define temp (cons '() '()))


(define a (list 1 2 3))

(define b (cons temp (cons temp '())))


(define temp2 (cons temp temp))

(define c (cons temp2 temp2))

(define d (list '() '() '()))
(set-car! d d)

(count-pairs a)
(count-pairs b)
(count-pairs c)
(count-pairs d)


;; Further testing!

(define f (list 1 2 3 4 5 6))

(count-pairs f)

