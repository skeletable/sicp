(define (make-f)
  (define state 0) (lambda (x) (let ((return state)) (begin (set! state x)) return)))

(define f (make-f))


(+ (f 0) (f 1))

(define f (make-f))

(+ (f 1) (f 0))

;; left->right: 0
;; right->left: 1
git 
