(define (make-queue)
  (let ((front-ptr '()) (rear-ptr '()))
    (define (empty-queue?) (null? front-ptr))
    (define (front-queue) front-ptr)
    (define (insert-queue! x) (cond ((null? front-ptr)
                                     (let ((k (list x)))
                                       (set! front-ptr k)
                                       (set! rear-ptr k)
                                       front-ptr))
                                    (else (let ((k (cons x '()))) (set-cdr! rear-ptr k) (set! rear-ptr k) front-ptr))))
    (define (delete-queue!) (set! front-ptr (cdr front-ptr)) front-ptr)

    (define (dispatch m) (cond ((eq? m 'empty-queue?) empty-queue)
                               ((eq? m 'front-queue) front-queue)
                               ((eq? m 'insert-queue) insert-queue!)
                               ((eq? m 'delete-queue) delete-queue!)))
    dispatch))


(define q (make-queue))

(q 'empty-queue)

(q 'front-queue)

((q 'insert-queue) 'a)

((q 'insert-queue) 'b)

((q 'insert-queue) 'c)

((q 'delete-queue))

((q 'delete-queue))


