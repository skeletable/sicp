(define (integrate-series s) (map-stream / integrate-series integers))

(define exp-series
  (cons-stream 1 (integrate-series exp-series)))

(define cosine-series (stream-cons 1 (map-stream - (integrate-series sine-series))))
(define sine-series (stream-cons 1 (integrate-series cosine-series)))
