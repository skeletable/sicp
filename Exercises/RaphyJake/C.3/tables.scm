(define (lookup key table)
  (let ((record (assoc key (cdr table))))
    (if record
        (cdr record)
        #f)))

(define (assoc key records)
  (cond ((null? records) #f)
      ((eq? key (caar records)) (car records))
      (else (assoc key (cdr records)))))



(define (make-table)
  (define table (list '*table*))

  (define (lookup key-1 key-2)
    (let ((subtable (assoc key-1 (cdr table))))
      (if subtable
          (let ((record (assoc key-2 (cdr subtable))))
            (if record (car record) #f) )
          #f)))

  (define (insert! key-1 key-2 value)
    (let ((subtable (assoc key-1 (cdr table))))
      (if subtable
          (let ((record (assoc key-2 (cdr subtable))))
                (if record (set-cdr! record value)
                    (set-cdr! subtable (cons
                                        (cons key-2 value)
                                        (cdr subtable)))))
          (set-cdr! table
                    (cons (list key-1 (cons key-2 value)) (cdr table))))))


  (define (dispatch m) (cond ((eq? m 'insert) insert!)
                             ((eq? m 'lookup) lookup)))

  dispatch)

(define q (make-table))

((q 'insert) 'to-do 'school 'eat-crap)
((q 'lookup) 'to-do 'school)

((q 'insert) 'to-do 'school 'eat-pussy)
((q 'lookup) 'to-do 'school)

((q 'insert) 'to-do 'gatorade 'get-paid)
((q 'insert) 'to-do 'gatorade 'get-laid)
((q 'insert) 'to-sex 'your-mom 'done)
((q 'insert) 'to-sex 'elisa 'not-done)


((q 'lookup) 'to-do 'gatorade)
((q 'lookup) 'to-sex 'elisa)
((q 'lookup) 'to-sex 'your-mom)

