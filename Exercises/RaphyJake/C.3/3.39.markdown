```
(define x 10)

(define s (make-serializer))

(parallel-execute (lambda () (set! x ((s (lambda () (* x x))))))
                  (s (lambda () (set! x (+ x 1)))))

```


We are only assured that during the two accesses of P1 of x, the value doesn't get modified.

So, in other words, we can still get:

101
121 ;;The two cases with no interleaving
11
100
