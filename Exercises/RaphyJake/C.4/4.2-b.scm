;; The first eval-apply we get to see.

(define (eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((application? exp) (apply (eval (operator exp) env) ;; Application is now on top!
                                   (list-of-values (operand exp) env)))
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp) (lambda-body exp) env))
        ((begin? exp) (eval-sequence (begin-actions exp) env))
        ((cond? exp) (eval (cond-if exp) env))

        (else (display "FUCK IT"))
        ))


(define (apply procedure arguments)
  (cond ((primitive-procedure? procedure) (apply-primitive-procedure procedure arguments))
        ((compound-procedure? procedure)
         (eval-sequence (procedure-body procedure)
                        (extend-environment (procedure-parameters procedure)
                                            arguments
                                            (procedure-environment procedure))))
        (else (display "FUCK IT!!!"))))

(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      (cons (eval (first-operand exps) env)
            (list-of-values (rest-operands exps) env))))

(define (eval-if exp env)
  (if (true? (eval (if-predicate exp) env))
      (eval (if-consequent exp) env)
      (eval (if-alternative exp) env)))

(define (eval-sequence exps env)
  (cond ((last-exp? exps) (eval (first-exp exps) env))
        (else (eval (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))

(define (eval-assignment exp env)
  (set-variable-value! (assignment-variable exp)
                       (eval (definition-value exp) env)
                       env))

(define (eval-definition exp env)
  (set-variable-value! (definition-variable exp)
                       (eval (definition-value exp) env)
                       env))


(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? tag (car exp))
      #f))




(define (self-evaluating? exp)
  (cond ((symbol? exp) #t)
        ((number? exp) #t)
        (else #f)))

(define (variable? exp) (symbol? exp))


;;Quotations

(define (quoted? exp) (taggest-list? exp 'quote))
(define (text-of-quotation exp) (cadr exp))


;;Assignment

(define (assignment? exp) (tagged-list? exp 'set!))
(define (assignment-variable exp) (cadr exp))
(dfine (assignment-value exp) (caddr exp))


;;Definition

(define (definition? exp) (tagged-list? exp 'define))

(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)
      (caadr exp)))

(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lamba (cdadr exp) ;; Formal parameters
                  (cddr exp)) ;; Body
      ))


;;Lambda expressions

(define (lambda? exp) (tagged-list? exp 'lambda))

(define (lambda-parameters exp) (cadr exp))

(define (lambda-body? exp) (cddr exp))

(define (make-lambda parameters body)
  (list 'lambda parameters body))


;;Conditionals

(define (if? exp) (tagged-list? exp 'if))

(define (if-predicate exp) (cadr exp))

(define (if-consequent exp) (caddr exp))

(define (if-alternative exp) (cadddr exp))  ;; Should we support ifs with no alternative?

(define (make-if predicate consequent alternative)
  (list 'if predicate consequent alternative))


;;Begin

(define (begin? exp) (tagged-list? exp 'begin))

(define (begin-actions exp) (cdr exp))

(define (last-exp? seq) (null? cdr seq))

(define (first-exp seq) (car seq))

(define (rest-exps seq) (cdr seq))


(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))


(define (make-begin seq) (cons 'begin seq))


;;Procedures - Louis version

(define (application? exp) (tagged-list? exp 'call))
(define (operator exp) (cadr exp))
(define (operands exp) (cddr exp))
(define (no-operands? ops) (null? ops))
(define (first-operand ops) (car ops))
(define (rest-operands ops) (cdr ops))


;;Switch case, or cond

(define (cond? exp)
  (tagged-list? exp 'cond))

(define (cond-clauses exp) (cdr exp))
(define (cond-else-clause? clause) (eq? (cond-predicate clause) 'else))

(define (cond-predicate clause) (car clause))

(define (cond-actions clause) (cdr clause))

(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))

(define (expand-clauses clauses)
  (if (null? clauses)
      'false     ; no else clause
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp
                 (cond-actions first))
                (display "ELSE clause isn't last: COND->IF"))
            (make-if (cond-predicate first)
                     (sequence->exp
                      (cond-actions first))
                     (expand-clauses
                      rest))))))
