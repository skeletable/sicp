```
(define (square x) (* x x))

(square (id 10))

count
```

With memoization, this is just 1. Without, (id 10) gets called twice inside (* x x), making count = 2
