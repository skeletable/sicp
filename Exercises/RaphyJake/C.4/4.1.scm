(define (list-of-values-r-l exps env)
  (if (no-operands? exps)
      '()
      (let ((first '()) (rest '()))
        (begin (set! first (eval (first-operand exps) env))
               (set! rest (list-of-values-r-l (rest-operands exps) env))
               (cons first rest)
               ))))


(define (list-of-values-l-r exps env)
  (if (no-operands? exps)
      '()
      (let ((first '()) (rest '()))
        (begin (set! rest (list-of-values-l-r (rest-operands exps) env))
               (set! first (eval (first-operand exps) env))
               (cons first rest)
               ))))

