Alyssa's version of analyze sequence has still to do some syntactic analysis at runtime!
In our version, analyze-sequence just returns one lambda expression, where env must be applied. 
In alyssa's version, we run that execute-sequence part recursively, which does syntax analysis. This is slower. 
