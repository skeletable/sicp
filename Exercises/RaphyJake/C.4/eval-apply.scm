(define debug-analyze #f)
(define debug-lookup #f)
(define debug-application #f)

(define apply-in-underlying-scheme apply) ;; Saving this for later


(define (make-table)  ;; A quick table implementation.
  (define table '())

  (define (assoc key current)
      (cond ((null? current) #f)
            ((eq? (caar current) key) (car current))
            (else (assoc key (cdr current)))))

  (define (lookup key)
    (let ((record (assoc key table)))
      (if record (cdr record) #f)))

  (define (insert! key value)
    (let ((place (assoc key table)))
      (if place
          (set-cdr! place value)
          (set! table (cons (cons key value) table)))))


  (define (dispatch m)
    (cond ((eq? m 'lookup) lookup)
          ((eq? m 'insert!) insert!)
          (else (display "f you"))
          ))
  dispatch)

(define optable (make-table))
(define (add-op op fun) ((optable 'insert!) op fun))
(define (get-op op) ((optable 'lookup) op))


(define (eval exp env)
  ((analyze exp) env))

;; analyze

(define (analyze exp) (cond (debug-analyze (display "ANALIZIN':") (display exp) (newline)))
  (cond ((self-evaluating? exp) (analyze-self-evaluating exp))
        ((variable? exp) (analyze-variable exp))
        ((get-op (car exp)) ((get-op (car exp)) exp))
        ((application? exp) (analyze-application exp))
        (else (display "FUCK IT!"))
        ))

(define (analyze-self-evaluating exp) (lambda (en) exp))

(define (analyze-quoted exp)
  (let ((qval (text-of-quotation exp)))
    (lambda (en) qval)))

(define (analyze-variable exp)
  (lambda (en) (lookup-variable-value exp en)))

(define (analyze-assignment exp)
  (let ((var (assignment-variable exp))
        (vproc (analyze (assignment-value exp))))
    (lambda (en) (set-variable-value! var (vproc en) en)
  'ok)))

(define (analyze-definition exp)
  (let ((var (definition-variable exp))
        (vproc (analyze (definition-value exp))))
    (lambda (en) (define-variable! var (vproc en) en)
  'ok)))

(define (analyze-if exp)
  (let ((pproc (analyze (if-predicate exp)))
        (cproc (analyze (if-consequent exp)))
        (aproc (analyze (if-alternative exp))))
    (lambda (en)
      (if (true? (actual-value pproc en))
          (cproc en)
          (aproc en)))))

(define (analyze-lambda exp)
  (let ((vars (lambda-parameters exp))
        (bproc (analyze-sequence (lambda-body exp))))
    (lambda (en) (make-procedure vars bproc en))))

(define (analyze-sequence exps)
  (define (sequentially proc1 proc2)
    (lambda (env) (proc1 env) (proc2 env)))
  (define (loop first-proc rest-procs)
    (if (null? rest-procs)
        first-proc
        (loop (sequentially first-proc (car rest-procs))
              (cdr rest-procs))))
  (let ((procs (map analyze exps)))
    (if (null? procs)
        (display "Empty sequence: ANALYZE"))
    (loop (car procs) (cdr procs))))

(define (analyze-pair exp)
  (let ((xproc (analyze (first-element exp)))
        (yproc (analyze (second-element exp))))
    (lambda (en) (make-pair (delay-it xproc en) (delay-it yproc en)))))

(define (analyze-car exp)
  (let ((pproc (analyze (car-pair exp))))
    (lambda (en) (first-force (force-it (pproc en))))))

(define (analyze-cdr exp)
  (let ((pproc (analyze (cdr-pair exp))))
    (lambda (en) (second-force (force-it (pproc en))))))


(define (analyze-application exp)
  (let ((fproc (analyze (operator exp)))
        (aprocs (map analyze (operands exp))))
    (lambda (env)
      (execute-application
       (actual-value fproc env)
       aprocs env))))


(define (execute-application proc arguments env)
  (cond (debug-application (display "proc:") (user-print proc) (newline)
         (display "args:") (user-print arguments) (newline)))
  (cond ((primitive-procedure? proc)
         (cond (debug-application (display "strict args:")
                                  (user-print (list-of-args-values arguments env)) (newline))) ;;debug
         (apply-primitive-procedure proc (list-of-args-values arguments env)))
         ((compound-procedure? proc)
          (cond (debug-application (display "delayed args:")
                                   (user-print (list-of-delayed-args arguments env)) (newline))) ;;debug
         ((procedure-body proc)
          (extend-environment (procedure-parameters proc) (list-of-delayed-args arguments env)
           (procedure-environment proc))))
        (else (display "Unknown procedure type:
                      EXECUTE-APPLICATION"
                     proc))))



;; lazy shit

(define (actual-value exp env)
  (force-it (exp env)))

(define (actual-value-input exp env)
  (force-it (eval exp env)))

(define (force-it obj)
  (cond ((thunk? obj)
         (let ((result
                (actual-value
                 (thunk-exp obj)
                 (thunk-env obj))))
           (set-car! obj 'evaluated-thunk)
           ;; replace exp with its value:
           (set-car! (cdr obj) result)
           ;; forget unneeded env:
           (set-cdr! (cdr obj) '())
           result))
        ((evaluated-thunk? obj)
         (thunk-value obj))
        (else obj)))

(define (delay-it exp env)
  (list 'thunk exp env))
(define (thunk? obj) (tagged-list? obj 'thunk))
(define (thunk-exp thunk) (cadr thunk))
(define (thunk-env thunk) (caddr thunk))

(define (evaluated-thunk? obj)
  (tagged-list? obj 'evaluated-thunk))

(define (thunk-value evaluated-thunk)
  (cadr evaluated-thunk))


(define (list-of-args-values exps env)
  (if (no-operands? exps)
      '()
      (cons (actual-value (first-operand exps) env)
            (list-of-args-values (rest-operands exps) env))))

(define (list-of-delayed-args exps env)
  (if (no-operands? exps)
      '()
      (cons (delay-it (first-operand exps) env)
            (list-of-delayed-args (rest-operands exps) env))))


(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? tag (car exp))
      #f))



(define (self-evaluating? exp)
  (cond ((string? exp) #t)
        ((number? exp) #t)
        (else #f)))

(define (variable? exp) (symbol? exp))


;;; Lazy Pairs

;; 'cons <x> <y>
(define cons-symbol 'cons)
(define (first-element pair) (cadr pair))
(define (second-element pair) (caddr pair))

(define (first-force pair) (force-it (first-element pair)))
(define (second-force pair) (force-it (second-element pair)))

(define (cons? exp) (tagged-list? exp cons-symbol))

(define (make-pair x y) (list cons-symbol x y))

;; 'car <pair>
(define car-symbol 'car)
(define (car-pair pair) (cadr pair))

(define (make-car pair) (list car-symbol pair))

;; 'cdr <pair>
(define cdr-symbol 'cdr)
(define (cdr-pair pair) (cadr pair))

(define (make-cdr pair) (list cdr-symbol pair))



;;; Quotations

;; 'quote <text-of-quotation>

(define (text-of-quotation exp) (cadr exp))


;;; Assignment 'set!

;; 'set! <variable> <value>

(define set-symbol 'set!)
(define (make-set variable value) (list set-symbol variable value))

(define (assignment-variable exp) (cadr exp))
(define (assignment-value exp) (caddr exp))


;;Definition 'define

;; 'define <variable> <value>
;; or
;; 'define (<procedure-name> <formal parameters>) <body>

(define define-symbol 'define)
(define (make-define-variable variable value) (list define-symbol variable value))
(define (make-define-fun proc-name parameters body) (append (list define-symbol (cons proc-name parameters)) body))

(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)
      (caadr exp)))

(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lambda (cdadr exp) ;; Formal parameters
                  (cddr exp)) ;; Body
      ))


;;Lambda expressions

;; 'lambda (<formal parameters>) <body>

(define lambda-symbol 'lambda)
(define (make-lambda parameters body)
  (cons lambda-symbol (cons parameters body)))

(define (lambda-parameters exp) (cadr exp))

(define (lambda-body exp) (cddr exp))


;;Let expressions

;; 'let ((<variable> <expression>)) <body>
;; or
;; 'let <variable> ((<variable> <expression>)) <body>

(define let-symbol 'let)
(define (make-let bindings body) (append (list let-symbol bindings) body))
(define (make-named-let var bindings body) (list let-symbol bindings body))

(define (named-let? exp) (symbol? (cadr exp)))
(define (let-variables exp) (map car (if (named-let? exp) (caddr exp) (cadr exp))))
(define (let-expressions exp) (map cadr (if (named-let? exp) (caddr exp) (cadr exp))))
(define (let-body exp) (if (named-let? exp) (cdddr exp) (cddr exp)))
(define (named-let-variable exp) (cadr exp))

(define (let->combination exp)
  (if (named-let? exp)
      (make-begin (list (make-define-fun (named-let-variable exp) (let-variables exp) (let-body exp))
                        (cons (named-let-variable exp) (let-expressions exp))))
      (cons (make-lambda (let-variables exp) (let-body exp)) (let-expressions exp))))

;;Let* expression. Nice!

;; 'let* ((<variable> <expression)) <body>

(define let*-symbol 'let*)
(define (make-let* bindings body) (append (list let-symbol bindings) body))

(define (let*-variables exp) (map car (cadr exp)))
(define (let*-expressions exp) (map cadr (cadr exp)))
(define (let*-body exp) (cddr exp))

(define (let*->lets exp)
  (expand-let* (let*-variables exp) (let*-expressions exp) (let*-body exp)))


(define (expand-let* variables expressions body)
  (if (or (null? (cdr variables)) (null? (cdr expressions))) ;; these should always be the same anyway..
      (make-let (list (list (car variables) (car expressions)))
                body)
      (make-let (list (list (car variables) (car expressions)))
                (list (expand-let* (cdr variables) (cdr expressions) body)))
      ))

;;Conditionals

;; 'if <condition> <consequent> <alternative>

(define if-symbol 'if)

(define (make-if predicate consequent alternative)
  (list if-symbol predicate consequent alternative))

(define (if-predicate exp) (cadr exp))
(define (if-consequent exp) (caddr exp))
(define (if-alternative exp) (cadddr exp))  ;; Should we support ifs with no alternative?


;;Begin 'begin

;; 'begin <expressions>

(define begin-symbol 'begin)
(define (make-begin seq) (cons begin-symbol seq))

(define (begin-actions exp) (cdr exp))
(define (last-exp? seq) (null? (cdr seq)))
(define (first-exp seq) (car seq))
(define (rest-exps seq) (cdr seq))


(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))


;;Switch case, or cond 'cond

;; 'cond ((<condition> <consequent))
;; or
;; 'cond ((<condition> => <function>))
;; or
;; 'cond ((else <consequent>))

(define cond-symbol 'cond)
(define else-symbol 'else)
(define (make-cond clauses) (cons cond-symbol clauses))

(define (cond-clauses exp) (cdr exp))
(define (cond-else-clause? clause) (eq? (cond-predicate clause) else-symbol))

(define (cond-predicate clause) (car clause))
(define (cond-special-syntax? clause) (eq? (cadr clause) '=>))
(define (cond-special-syntax-function clause) (caddr clause))
(define (cond-actions clause) (cdr clause))

(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))

(define (expand-clauses clauses)
  (if (null? clauses)
      'false     ; no else clause
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp
                 (cond-actions first))
                (display "ELSE clause isn't last: COND->IF"))
            (if (cond-special-syntax? first)
                (list (make-lambda (list 'x)
                                   (list (make-if 'x
                                            (list (cond-special-syntax-function first) 'x)
                                            (expand-clauses rest))))
                      (cond-predicate first))
                (make-if (cond-predicate first)
                         (sequence->exp
                          (cond-actions first))
                         (expand-clauses
                          rest)))))))

;;Or and And

;; 'or <expressions> <expression> ..

;; 'and <expression> <expression> ..

(define or-symbol 'or)
(define and-symbol 'and)
(define (make-or expressions) (cons or-symbol expressions))
(define (make-and expressions) cons and-symbol expressions)

(define (or-and-clauses exp) (cdr exp))
(define (or-and-last-clause? seq) (null? (cdr seq)))

(define (expand-and->if clauses)
  (if (null? clauses)
      'false
      (if (or-and-last-clause? clauses)
          (car clauses) ; let's pretend a single clause OR or AND makes sense
          (make-if (car clauses)
                   (expand-and->if (cdr clauses))
                   'false)
          )))

(define (expand-or->if clauses)
  (if (null? clauses)
      'false
      (if (or-and-last-clause? clauses)
          (car clauses) ; let's pretend a single clause OR or AND makes sense
          (make-if (car clauses)
                   'true
                   (expand-or->if (cdr clauses)))
          )))

(define (or->if exp) (expand-or->if (cdr exp)))
(define (and->if exp) (expand-and->if (cdr exp)))

;; Conditionals

(define (true? x)
  (not (eq? x #f)))


(define (false? x)
  (eq? x #f))

;; Generic procedures


(define (application? exp) (pair? exp))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))
(define (no-operands? ops) (null? ops))
(define (first-operand ops) (car ops))
(define (rest-operands ops) (cdr ops))


(define (make-procedure parameters body env)
  (list 'procedure parameters body env))

(define (compound-procedure? p) (tagged-list? p 'procedure))

(define (procedure-parameters p) (cadr p))

(define (procedure-body p) (caddr p))

(define (procedure-environment p) (cadddr p))


;; Optable

(add-op car-symbol analyze-car)
(add-op cdr-symbol analyze-cdr)
(add-op cons-symbol analyze-pair)


(add-op let*-symbol (lambda (ex) (analyze (let*->lets ex))))
(add-op let-symbol (lambda (ex) (analyze (let->combination ex))))
(add-op and-symbol (lambda (ex) (analyze (and->if ex))))
(add-op or-symbol (lambda (ex) (analyze (or->if ex))))
(add-op cond-symbol (lambda (ex) (analyze (cond->if ex))))
(add-op begin-symbol (lambda (ex) (analyze-sequence (begin-actions ex))))
(add-op lambda-symbol analyze-lambda)
(add-op if-symbol analyze-if)
(add-op define-symbol analyze-definition)
(add-op set-symbol analyze-assignment)
(add-op 'quote analyze-quoted)


(define primitive-procedures

  (list (list 'null? null?)
        (list 'eq? eq?)
        (list '+ +)
        (list '- -)
        (list '* *)
        (list '= =)
        ))


;; ENVIRONMENT

(define (enclosing-environment env) (cdr env))
(define (first-frame env) (car env))
(define the-empty-environment '())

(define (make-frame variables values)
  (cons variables values))

(define (frame-variables frame) (car frame))

(define (frame-values frame) (cdr frame))

(define (add-binding-to-frame! var val frame)
  (set-car! frame (cons var (car frame)))
  (set-cdr! frame (cons val (cdr frame))))


(define (extend-environment vars vals base-env)
  (if (= (length vars) (length vals))
      (cons (make-frame vars vals) base-env)
      (if (< (length vars) (length vals))
          (display "Too many arguments supplied")
          (display "Too few arguments supplied"))))


(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop
              (enclosing-environment env)))
            ((eq? var (car vars))
             ;(cond (debug-lookup (display "LOOKUP: found: ") (user-print (car vals)) (newline)))
             (car vals))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (if (eq? env the-empty-environment)
        (begin (display "Unbound variable: ") (display var) (newline))
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (cond (debug-lookup (display "LOOKUP: looking up: ") (display var) (newline)))
  (env-loop env))

(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop
              (enclosing-environment env)))
            ((eq? var (car vars))
             (set-car! vals val))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (if (eq? env the-empty-environment)
        (display "Unbound variable: SET!")
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (env-loop env))

(define (define-variable! var val env)
  (let ((frame (first-frame env)))
    (define (scan vars vals)
      (cond ((null? vars)
             (add-binding-to-frame!
              var val frame))
            ((eq? var (car vars))
             (set-car! vals val))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (scan (frame-variables frame)
          (frame-values frame))))

;; ENV

(define (primitive-procedure-objects)
  (map (lambda (proc)
         (list 'primitive (cadr proc)))
       primitive-procedures))


(define (primitive-procedure-names)
  (map car primitive-procedures))

(define (setup-environment)
  (let ((initial-env
         (extend-environment
          (primitive-procedure-names)
          (primitive-procedure-objects)
          the-empty-environment)))
    (define-variable! 'true #t initial-env)
    (define-variable! 'false #f initial-env)
    initial-env))


(define the-global-environment
  (setup-environment))


(define (primitive-procedure? proc)
  (tagged-list? proc 'primitive))


(define (primitive-implementation proc)
  (cadr proc))



(define (apply-primitive-procedure proc args)
  (apply-in-underlying-scheme
   (primitive-implementation proc) args))


(define input-prompt  ";;; L-Eval input:")

(define output-prompt ";;; L-Eval value:")



(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
    (let ((output
           (actual-value-input input
                 the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))



(define (prompt-for-input string)
  (newline) (newline)
  (display string) (newline))



(define (announce-output string)
  (newline) (display string) (newline))


(define (user-print object)
  (display (prettify object)))

(define (prettify obj)
  (cond ((compound-procedure? obj)
         (list 'compounu-ud-procedure
               (procedure-parameters obj)
               (procedure-body obj)
               '<procedure-env>))
        ((thunk? obj)
         (display "thrunk:")(prettify (force-it obj)))
        ((evaluated-thunk? obj)
         (display "thrunk:")(prettify (thunk-value obj)))
        ((cons? obj)
         (list-transformer obj))
         ;(list (prettify (first-element obj)) (prettify (second-element obj))))
        ((list? obj) (map prettify obj))
        (else obj)))

(define (list-transformer l-list)
  (define count 100)
  (define (loop slow fast)
    (let ((value (force-it fast)))
    (cond ((eq? count 0)
           'list-too-long)
          ((not (and (list? (force-it (second-element value))) (cons? (force-it (second-element value)))))
           (cons (first-force value)
                 (second-force value)))
          ((eq? slow fast)
           (cons (first-force value)
                 (cons (first-force (second-force value))
                       'infinite-list)))
          (else (set! count (- count 1)) (display count) (newline)
                (cons (first-force value)
                      (cons (first-force (second-force value))
                            (loop (second-element slow) (second-element (second-force value)))))))))
  (if (not (pair? (second-force l-list)))
      (cons (first-force l-list) (second-force l-list))
      (cons (first-force l-list) (loop l-list (second-element l-list)))))


                                        ;(cond ((null? l-list) '())
;      ((not (cons? l-list)) (prettify l-list))
;     (else (cons (prettify (first-element l-list))
;                  (list-transformer (second-element l-list))))))


(driver-loop)


