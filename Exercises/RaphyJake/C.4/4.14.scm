(define apply-in-underlying-scheme apply) ;; Saving this for later


(define (make-table)  ;; A quick table implementation.
  (define table '())

  (define (assoc key current)
      (cond ((null? current) #f)
            ((eq? (caar current) key) (car current))
            (else (assoc key (cdr current)))))

  (define (lookup key)
    (let ((record (assoc key table)))
      (if record (cdr record) #f)))

  (define (insert! key value)
    (let ((place (assoc key table)))
      (if place
          (set-cdr! place value)
          (set! table (cons (cons key value) table)))))


  (define (dispatch m)
    (cond ((eq? m 'lookup) lookup)
          ((eq? m 'insert!) insert!)
          (else (display "f you"))
          ))
  dispatch)

(define optable (make-table))
(define (add-op op fun) ((optable 'insert!) op fun))
(define (get-op op) ((optable 'lookup) op))


(define (eval exp env) (display exp) (newline)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))

        ((get-op (car exp)) ((get-op (car exp)) exp env))

        ((application? exp) (new-apply (eval (operator exp) env)
                                       (list-of-values (operands exp) env)))
        (else (display "FUCK IT ") (display exp) (newline))
        ))


(define (new-apply procedure arguments)
  (cond ((primitive-procedure? procedure) (apply-primitive-procedure procedure arguments))
        ((compound-procedure? procedure)
         (eval-sequence (procedure-body procedure)
                        (extend-environment (procedure-parameters procedure)
                                            arguments
                                            (procedure-environment procedure))))
        (else (display "FUCK IT!!!") (pretty-print procedure)
              )))



(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      (cons (eval (first-operand exps) env)
            (list-of-values (rest-operands exps) env))))

(define (eval-if exp env)
  (if (true? (eval (if-predicate exp) env))
      (eval (if-consequent exp) env)
      (eval (if-alternative exp) env)))

(define (eval-sequence exps env)
  (cond ((last-exp? exps) (eval (first-exp exps) env))
        (else (eval (first-exp exps) env)
              (eval-sequence (rest-exps exps) env))))

(define (eval-assignment exp env)
  (set-variable-value! (assignment-variable exp)
                       (eval (definition-value exp) env)
                       env)
  'ok)

(define (eval-definition exp env)
  (define-variable! (definition-variable exp)
                       (eval (definition-value exp) env)
                       env)
  'ok)


(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? tag (car exp))
      #f))



(define (self-evaluating? exp)
  (cond ((string? exp) #t)
        ((number? exp) #t)
        (else #f)))

(define (variable? exp) (symbol? exp))

;;; Quotations

;; 'quote <text-of-quotation>

(define (text-of-quotation exp) (cadr exp))



;;; Assignment 'set!

;; 'set! <variable> <value>

(define set-symbol 'set!)
(define (make-set variable value) (list set-symbol variable value))

(define (assignment-variable exp) (cadr exp))
(define (assignment-value exp) (caddr exp))


;;Definition 'define

;; 'define <variable> <value>
;; or
;; 'define (<procedure-name> <formal parameters>) <body>

(define define-symbol 'define)
(define (make-define-variable variable value) (list define-symbol variable value))
(define (make-define-fun proc-name parameters body) (append (list define-symbol (cons proc-name parameters)) body))

(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)
      (caadr exp)))

(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lambda (cdadr exp) ;; Formal parameters
                  (cddr exp)) ;; Body
      ))


;;Lambda expressions

;; 'lambda (<formal parameters>) <body>

(define lambda-symbol 'lambda)
(define (make-lambda parameters body)
  (cons lambda-symbol (cons parameters body)))

(define (lambda-parameters exp) (cadr exp))

(define (lambda-body exp) (cddr exp))


;;Let expressions

;; 'let ((<variable> <expression>)) <body>
;; or
;; 'let <variable> ((<variable> <expression>)) <body>

(define let-symbol 'let)
(define (make-let bindings body) (append (list let-symbol bindings) body))
(define (make-named-let var bindings body) (list let-symbol bindings body))

(define (named-let? exp) (symbol? (cadr exp)))
(define (let-variables exp) (map car (if (named-let? exp) (caddr exp) (cadr exp))))
(define (let-expressions exp) (map cadr (if (named-let? exp) (caddr exp) (cadr exp))))
(define (let-body exp) (if (named-let? exp) (cdddr exp) (cddr exp)))
(define (named-let-variable exp) (cadr exp))

(define (let->combination exp)
  (if (named-let? exp)
      (make-begin (list (make-define-fun (named-let-variable exp) (let-variables exp) (let-body exp))
                        (cons (named-let-variable exp) (let-expressions exp))))
      (cons (make-lambda (let-variables exp) (let-body exp)) (let-expressions exp))))

;;Let* expression. Nice!

;; 'let* ((<variable> <expression)) <body>

(define let*-symbol 'let*)
(define (make-let* bindings body) (append (list let-symbol bindings) body))

(define (let*-variables exp) (map car (cadr exp)))
(define (let*-expressions exp) (map cadr (cadr exp)))
(define (let*-body exp) (cddr exp))

(define (let*->lets exp)
  (expand-let* (let*-variables exp) (let*-expressions exp) (let*-body exp)))


(define (expand-let* variables expressions body)
  (if (or (null? (cdr variables)) (null? (cdr expressions))) ;; these should always be the same anyway..
      (make-let (list (list (car variables) (car expressions)))
                body)
      (make-let (list (list (car variables) (car expressions)))
                (list (expand-let* (cdr variables) (cdr expressions) body)))
      ))

;;Conditionals

;; 'if <condition> <consequent> <alternative>

(define if-symbol 'if)

(define (make-if predicate consequent alternative)
  (list if-symbol predicate consequent alternative))

(define (if-predicate exp) (cadr exp))
(define (if-consequent exp) (caddr exp))
(define (if-alternative exp) (cadddr exp))  ;; Should we support ifs with no alternative?



;;Begin 'begin

;; 'begin <expressions>

(define begin-symbol 'begin)
(define (make-begin seq) (cons begin-symbol seq))

(define (begin-actions exp) (cdr exp))
(define (last-exp? seq) (null? (cdr seq)))
(define (first-exp seq) (car seq))
(define (rest-exps seq) (cdr seq))


(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))




;;Switch case, or cond 'cond

;; 'cond ((<condition> <consequent))
;; or
;; 'cond ((<condition> => <function>))
;; or
;; 'cond ((else <consequent>))

(define cond-symbol 'cond)
(define else-symbol 'else)
(define (make-cond clauses) (cons cond-symbol clauses))

(define (cond-clauses exp) (cdr exp))
(define (cond-else-clause? clause) (eq? (cond-predicate clause) else-symbol))

(define (cond-predicate clause) (car clause))
(define (cond-special-syntax? clause) (eq? (cadr clause) '=>))
(define (cond-special-syntax-function clause) (caddr clause))
(define (cond-actions clause) (cdr clause))

(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))

(define (expand-clauses clauses)
  (if (null? clauses)
      'false     ; no else clause
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp
                 (cond-actions first))
                (display "ELSE clause isn't last: COND->IF"))
            (if (cond-special-syntax? first)
                (list (make-lambda (list 'x)
                                   (list (make-if 'x
                                            (list (cond-special-syntax-function first) 'x)
                                            (expand-clauses rest))))
                      (cond-predicate first))
                (make-if (cond-predicate first)
                         (sequence->exp
                          (cond-actions first))
                         (expand-clauses
                          rest)))))))

;;Or and And

;; 'or <expressions> <expression> ..

;; 'and <expression> <expression> ..

(define or-symbol 'or)
(define and-symbol 'and)
(define (make-or expressions) (cons or-symbol expressions))
(define (make-and expressions) cons and-symbol expressions)

(define (or-and-clauses exp) (cdr exp))
(define (or-and-last-clause? seq) (null? (cdr seq)))

(define (expand-and->if clauses)
  (if (null? clauses)
      'false
      (if (or-and-last-clause? clauses)
          (car clauses) ; let's pretend a single clause OR or AND makes sense
          (make-if (car clauses)
                   (expand-and->if (cdr clauses))
                   'false)
          )))

(define (expand-or->if clauses)
  (if (null? clauses)
      'false
      (if (or-and-last-clause? clauses)
          (car clauses) ; let's pretend a single clause OR or AND makes sense
          (make-if (car clauses)
                   'true
                   (expand-or->if (cdr clauses)))
          )))

(define (or->if exp) (expand-or->if (cdr exp)))
(define (and->if exp) (expand-and->if (cdr exp)))

;; Conditionals

(define (true? x)
  (not (eq? x #f)))


(define (false? x)
  (eq? x #f))

;; Generic procedures


(define (application? exp) (pair? exp))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))
(define (no-operands? ops) (null? ops))
(define (first-operand ops) (car ops))
(define (rest-operands ops) (cdr ops))


(define (make-procedure parameters body env)
  (list 'procedure parameters body env))

(define (compound-procedure? p) (tagged-list? p 'procedure))

(define (procedure-parameters p) (cadr p))

(define (procedure-body p) (caddr p))

(define (procedure-environment p) (cadddr p))


;; Optable

(add-op let*-symbol (lambda (ex en) (eval (let*->lets ex) en)))
(add-op let-symbol (lambda (ex en) (eval (let->combination ex) en)))
(add-op and-symbol (lambda (ex en) (eval (and->if ex) en)))
(add-op or-symbol (lambda (ex en) (eval (or->if ex) en)))
(add-op cond-symbol (lambda (ex en) (eval (cond->if ex) en)))
(add-op begin-symbol (lambda (ex en) (eval-sequence (begin-actions ex) en)))
(add-op lambda-symbol (lambda (ex en) (make-procedure (lambda-parameters ex) (lambda-body ex) en)))
(add-op if-symbol eval-if)
(add-op define-symbol eval-definition)
(add-op set-symbol eval-assignment)
(add-op 'quote (lambda (ex en) (text-of-quotation ex)))


(define primitive-procedures

  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'null? null?)
        (list 'eq? eq?)
        (list '+ +)
        (list '- -)
        (list '* *)
        (list '= =)
        (list 'map map)
        ))


;; ENVIRONMENT

(define (enclosing-environment env) (cdr env))
(define (first-frame env) (car env))
(define the-empty-environment '())

(define (make-frame variables values)
  (cons variables values))

(define (frame-variables frame) (car frame))

(define (frame-values frame) (cdr frame))

(define (add-binding-to-frame! var val frame)
  (set-car! frame (cons var (car frame)))
  (set-cdr! frame (cons val (cdr frame))))


(define (extend-environment vars vals base-env)
  (if (= (length vars) (length vals))
      (cons (make-frame vars vals) base-env)
      (if (< (length vars) (length vals))
          (display "Too many arguments supplied")
          (display "Too few arguments supplied"))))


(define (lookup-variable-value var env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop
              (enclosing-environment env)))
            ((eq? var (car vars))
             (car vals))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (if (eq? env the-empty-environment)
        (begin (display "Unbound variable: ") (display var) (newline))
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (env-loop env))

(define (set-variable-value! var val env)
  (define (env-loop env)
    (define (scan vars vals)
      (cond ((null? vars)
             (env-loop
              (enclosing-environment env)))
            ((eq? var (car vars))
             (set-car! vals val))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (if (eq? env the-empty-environment)
        (display "Unbound variable: SET!")
        (let ((frame (first-frame env)))
          (scan (frame-variables frame)
                (frame-values frame)))))
  (env-loop env))

(define (define-variable! var val env)
  (let ((frame (first-frame env)))
    (define (scan vars vals)
      (cond ((null? vars)
             (add-binding-to-frame!
              var val frame))
            ((eq? var (car vars))
             (set-car! vals val))
            (else (scan (cdr vars)
                        (cdr vals)))))
    (scan (frame-variables frame)
          (frame-values frame))))

;; ENV

(define (primitive-procedure-objects)
  (map (lambda (proc)
         (list 'primitive (cadr proc)))
       primitive-procedures))


(define (primitive-procedure-names)
  (map car primitive-procedures))

(define (setup-environment)
  (let ((initial-env
         (extend-environment
          (primitive-procedure-names)
          (primitive-procedure-objects)
          the-empty-environment)))
    (define-variable! 'true #t initial-env)
    (define-variable! 'false #f initial-env)
    initial-env))


(define the-global-environment
  (setup-environment))

(define (primitive-procedure? proc)
  (tagged-list? proc 'primitive))


(define (primitive-implementation proc)
  (cadr proc))



(define (apply-primitive-procedure proc args)
  (apply-in-underlying-scheme
   (primitive-implementation proc) args))


(define input-prompt  ";;; M-Eval input:")

(define output-prompt ";;; M-Eval value:")



(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
    (let ((output
           (eval input
                 the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))



(define (prompt-for-input string)
  (newline) (newline)
  (display string) (newline))



(define (announce-output string)
  (newline) (display string) (newline))


(define (user-print object)
  (if (compound-procedure? object)
      (display
       (list 'compound-procedure
             (procedure-parameters object)
             (procedure-body object)
             '<procedure-env>))
      (display object)))


(define the-global-environment
  (setup-environment))


(driver-loop)

