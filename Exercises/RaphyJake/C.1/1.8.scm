(define (cube x) (* x x x))

(define (square x) (* x x))

(define (cbrt-iter guess x)
   (if (good-enough? guess (improve guess x)) 
        guess
        (cbrt-iter (improve guess x) x)))

(define (improve guess x) 
    (/ 3 (+ (/ x (square guess))
            (* 2 guess)))

(define (good-enough? guess next-guess)
    (< (abs (- next-guess guess) ) (* guess 0.0001)))

(define (cbrt x) (cbrt-iter 1.0 x))

(cube (cbrt 1000))

(cube (cbrt 1.0))

(cube (cbrt 0.5))
(cube (cbrt 0.0004))

(cube 1234)
(cbrt 1522756)
(cbrt (cube 1234))