
(define (cont-frac n d k)
  (define (cont a)
    (if (= a k)
        1
        (/ (n a) (+ (d a) (cont (+ 1 a))))))
  (cont 1))

(define (it-cont-frac n d k)
  (define (cont a b)
    (if (= a 0)
        b
        (cont (- a 1) (/ (n a) (+ (d a) b )))))
  (cont k 0))

(define (e k)
  (define (d i)
    (if (= 2 (remainder i 3))
        (* 2 ( /(+ 1 i) 3))
        1))
  (+ 2 (it-cont-frac (lambda (x) 1.0)
                     d
                     k)))

(e 20)

