the procedure p is called 5 times for `12.45`: when sine is called, the argument is divided by three until it becomes smaller than 0.1:

```
12.34/3 = 4.05
4.05/3 = 1.35
1.35/3 = 0.45
0.45/3 = 0.15
0.15/3 = 0.05

```

This means that the procedure sine has a time and space complexity of o(log n) (base 3): tripling the size of the imput value only adds one step to the computation.
