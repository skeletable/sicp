
(define (* a b) 

    (define (even? x) (= 0 (remainder x 2)))

    (define (double x) (+ x x))
    (define (halve x) (/ x 2))

    (cond ((= b 1) a)
          ((even? b) (* (double a) (halve b)))
          (else (+ a (* a (- b 1)))))
)

(* 3 6)
(* 1 3)
(* 3 1)

(* 3232 3232)