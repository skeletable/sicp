(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
               (sum term (next a) next b))))

(define (integral f a b n)
  (define h (/ (- b a) n))

  (define (y k) (f (+ a (* k h))))

  (define (inc x) (+ 1 x))

  (define (even? x) (= (remainder x 2) 0))


  (define (term x)
    (cond ((= x 0) (y x))
          ((= x n) (y x))
          ((even? x) (* 2 (y x)))
          (else (* 4 (y x)))))

  (* (/ h 3.0)
     (sum term 0 inc n)
  )
)

(define (cube x) (* x x x))

(integral cube 0 1 100)
(integral cube 0 1 1000)
