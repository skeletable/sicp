
(define (fix f x tolerance)
  (define (close-enough? p1 p2) (< (abs (- p1 p2)) tolerance))

  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try x))

(fix cos 1 0.0001)

(define (sqrt x)
  (fix (lambda (y) (/ (+ y (/ x y)) 2)) 1.0 0.001)
  )

(sqrt 2)


(fix (lambda (x) (+ 1 (/ 1 x))) 1.0 0.000001)
