
(define (cont-frac n d k)
  (define (cont a)
    (if (= a k)
        1
        (/ (n a) (+ (d a) (cont (+ 1 a))))))
  (cont 1))

(cont-frac (lambda (x) 1.0)
           (lambda (x) 1.0)
           11)

;first procedure is "top down", iterative is "bottom up"

(define (it-cont-frac n d k)
  (define (cont a b)
    (if (= a 0)
        b
        (cont (- a 1) (/ (n a) (+ (d a) b )))))
  (cont k 0))

(it-cont-frac (lambda (x) 1.0)
              (lambda (x) 1.0)
              11)
