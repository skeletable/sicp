The code was changed from:

`1.6-1.7-1.8.scm`

```
(define (sqrt-iter guess x)
   (if (good-enough? guess x) 
        guess
        (sqrt-iter (improve guess x) x)))

(define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001 ))
```

To:

`1.7.scm`
```
(define (sqrt-iter guess x)
   (if (good-enough? guess (improve guess x)) 
        guess
        (sqrt-iter (improve guess x) x)))

(define (good-enough? guess next-guess)
    (< (abs (- next-guess guess) ) (* guess 0.0001)))
```

And the following tests were performed:

```
(square (sqrt 1000))

(square (sqrt 1.0))

(square (sqrt 0.5))
(square (sqrt 0.0004))

(square 1234)
(sqrt 1522756)
(sqrt (square 1234))
```

On `1.6-1.7-1.8.scm` (difference between x and the guess squared smaller than 0.001), they yielded:

```
1000.000369924366
1.0
0.5000015018262206
0.0012532224857331766
1522756
1234.0000000072437
1234.0000000072437
```

On `1.7.scm` (ratio between a guess and the next guess smaller than 1.0001), they yielded:

```
1000.000369924366
1.0
0.5000015018262206
4.000570666484372e-4
1522756
1234.004228172992
1234.004228172992
```

As we can see, using both metods, the 1000 test and the 0.5 test yield identical results. 
In the first test, the square root of `1234^2` is calculated with great accuracy = `1234.0000000072437`, but the square root of 0.004 is approximated awfully.

In general, using the first method, we can't calculate the square root of any number smaller than `0.001` accurately, since that's exactly our degree of accuracy on decimals, in general.

Using the second method, rather than fixing our correct decimal places (we cannot detect error smaller than 0.001 using the first `good-enough?`), we just ensure that the change in 
two consecutive guesses is small relative to the guess itself, thus getting a reasonably correct result according to the initial input. 
The second version performs much better at calculating `(sqare(sqrt 0.0004))`