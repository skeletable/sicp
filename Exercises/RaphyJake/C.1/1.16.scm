

(define (faster-exp a n) 

    (define (square x) (* x x))

    (define (even? x) (= 0 (remainder x 2)))

    (define (iter-exp base exponent accumulator)
        (cond ((= 0 exponent) accumulator)
              ((even? exponent) (iter-exp (square base) (/ exponent 2) accumulator))
              
              (else (iter-exp base (- exponent 1) (* accumulator base)))))

    (iter-exp a n 1)


)


(faster-exp 2 6)

(faster-exp 3 9)