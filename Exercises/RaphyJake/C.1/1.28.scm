
(define (fast-prime? n times)

  (define (square x) (* x x))

  (define (even? x) (= 0 (remainder x 2)))

  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
          (else (remainder (* base (expmod base (- exp 1) m)) m))))

  (define (milrab-test n)
    (define (try-it a) (= (expmod a (- n 1) n) (remainder 1 n)))
    (try-it (+ 1 (random (- n 1)))))

  (cond ((= times 0) #t)
        ((milrab-test n) (fast-prime? n (- times 1)))
        (else #f)))


(fast-prime? 19 100)
(fast-prime? 561 100)
(fast-prime? 97 100)
(fast-prime? 10000000000000079 100)
