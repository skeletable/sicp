
(define (fix f x tolerance)
  (define (close-enough? p1 p2) (< (abs (- p1 p2)) tolerance))

  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try x))



(define (root f guess dx)

  (define (deriv g)
    (lambda (x)
      (/ (- (g (+ x dx)) (g x)) dx)))

  (define (ntransform g)
    (lambda (x)
      (- x (/ (g x) ((deriv g) x)))))

  (fix (ntransform f) guess dx))

(define (cubic a b c)
  (define (square x ) (* x x))
  (define (cube x) (* x x x))
  (lambda (x) (+ (* a (cube x))
                 (* b (square x))
                 c)))

(root (cubic 4 2 -209) 21 0.0001)

