
(define (filter-accumulate combiner filter null-value term a next b)
  (if (> a b)
      null-value
      (if (filter a)
          (combiner (term a) (filter-accumulate combiner filter null-value term (next a) next b))
          (filter-accumulate combiner filter null-value term (next a) next b))))

(define (square x) (* x x))

(define (smallest-divisor n)

  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (next test-divisor)))))

  (define (divides? n m) (= 0 (remainder m n)))

  (define (next n) (if (= 2 n) 3 (+ 2 n)))

  (find-divisor n 2)

  )

(define (prime? n) (= n (smallest-divisor n)))



(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (inc x) (+ 1 x))
(define (id x) x)

(define (primesquare a b)
  (filter-accumulate * prime? 1 square a inc b))

(define (coprimeproduct a)
  (define (coprime? b) (= (gcd a b) 1))

  (filter-accumulate * coprime? 1 id 1 inc a))


(primesquare 1 10)

(coprimeproduct 10)


