
(define (cont-frac n d k)
  (define (cont a)
    (if (= a k)
        1
        (/ (n a) (+ (d a) (cont (+ 1 a))))))
  (cont 1))

(define (it-cont-frac n d k)
  (define (cont a b)
    (if (= a 0)
        b
        (cont (- a 1) (/ (n a) (+ (d a) b )))))
  (cont k 0))


(define (tan-cf x k)
  (it-cont-frac (lambda (i) (if (= i 1) x (- (* x x))))
                (lambda (i) (- (* 2 i) 1))
                k))

(tan-cf 3.14 10)
