
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a) (accumulate combiner null-value term (next a) next b))))

(define (it-accumulate combiner null-value term a next b)
  (define (iter-accumulate a accumulator)
    (if (> a b)
        accumulator
        (iter-accumulate (next a) (combiner accumulator (term a))))
    )
  (iter-accumulate a null-value)
  )

(define (sum term a next b)
  (accumulate + 0 term a next b))

(define (product term a next b)
  (it-accumulate * 1 term a next b))

(define (id x) x)
(define (inc x) (+ 1 x))

(sum id 1 inc 10)

(product id 1 inc 5)
