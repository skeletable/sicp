
(define (fast-prime? n times)

  (define (square x) (* x x))

  (define (even? x) (= 0 (remainder x 2)))

  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
          (else (remainder (* base (expmod base (- exp 1) m)) m))))

  (define (fermat-test n)
    (define (try-it a) (= (expmod a n n) a))
    (try-it (+ 1 (random (- n 1)))))

  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

(define (timed-prime-test n)
  ;(newline)
  ;(display n)
  (start-prime-test n (real-time)))


(define (start-prime-test n start-time)
  (if (fast-prime? n 100) (report-prime (- (real-time) start-time) n) #f))

(define (report-prime elapsed-time n)
  (newline)
  (display n)
  (display " *** ")
  (display elapsed-time))


(define (rangesearch3 min)
  (define (itersearch n)
    (if (timed-prime-test n) n (itersearch (+ 2 n))))

  (define (search min ) (itersearch (+ 2 (itersearch (+ 2 (itersearch min)) )) ))

  (define (even? n) (= 0 (remainder n 2)))

  (search (if (even? min) (+ 1 min) min))
)

(rangesearch3 1000)
(rangesearch3 10000)
(rangesearch3 100000)
(rangesearch3 100000000)
(rangesearch3 10000000000000000) ; i don't get it
