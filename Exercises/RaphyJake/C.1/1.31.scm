(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (it-sum term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (it-product term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (term a)))))
  (iter a 1))


(define (pi n)
  (define (inc x) (+ 1 x))

  (define (term n)
    (if (even? n)
        (/ (+ 2 n) (+ 1 n))
        (/ (+ 1 n) (+ 2 n))
        ))

  (* 4 (it-product term 1.0 inc n)))

  (define (approx-pi-with-term n)
  (define (pi-term n)
    (if (even? n)
        (/ (+ n 2) (+ n 1))
        (/ (+ n 1) (+ n 2))))
  (* 4 (product-iter pi-term 1 inc n)))



(pi 1000)
