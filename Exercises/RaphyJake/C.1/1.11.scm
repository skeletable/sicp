(define (f n) 
    (cond ((< n 3) n)
          (else (+ (f (- n 1))
                   (* 2 (f (- n 2)))
                   (* 3 (f (- n 3))))))))


(f 3)

(f 4)

(f 5)


(define (f2 n)
    (define (f2-iter a b c counter)
        (if (> counter 3)
            c
            (f2-iter (+ c (*2 b) (* 3 a)) b a (- counter 1))))

    (f2-iter 0 1 2 n)
)

(f 3)

(f 4)
(f 5)