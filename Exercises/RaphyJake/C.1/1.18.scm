
(define (* a b) 

    (define (even? x) (= 0 (remainder x 2)))

    (define (double x) (+ x x))
    (define (halve x) (/ x 2))

    (define (mult-iter a b c)
    (cond ((= b 1) (+ a c))
              ((even? b) (mult-iter (double a) (halve b) c))
              (else (mult-iter a (- b 1) (+ c a)))))

    (mult-iter a b 0)
)

(* 3 6)
(* 1 3)
(* 3 1)

(* 3232 3232)