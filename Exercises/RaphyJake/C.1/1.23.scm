
(define (smallest-divisor n)

  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (next test-divisor)))))

  (define (square x) (* x x))

  (define (divides? n m) (= 0 (remainder m n)))

  (define (next n) (if (= 2 n) 3 (+ 2 n)))

  (find-divisor n 2)

  )


(define (prime? n) (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (real-time)))


(define (start-prime-test n start-time)
  (if (prime? n) (report-prime (- (real-time) start-time)) #f))

(define (report-prime elapsed-time)
  ;(newline)
  ;(display n)
  (display " *** ")
  (display elapsed-time))


(define (rangesearch3 min)
  (define (itersearch n)
    (if (timed-prime-test n) n (itersearch (+ 2 n))))

  (define (search min ) (itersearch (+ 2 (itersearch (+ 2 (itersearch min)) )) ))

  (define (even? n) (= 0 (remainder n 2)))

  (search (if (even? min) (+ 1 min) min))
)

(rangesearch3 1000)
(rangesearch3 10000)
(rangesearch3 100000)
(rangesearch3 100000000)
(rangesearch3 10000000000000) ;performance is roughly doubled on my machine 
