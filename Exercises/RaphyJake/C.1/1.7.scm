(define (square x) (* x x))

(define (sqrt-iter guess x)
   (if (good-enough? guess (improve guess x)) 
        guess
        (sqrt-iter (improve guess x) x)))

(define (improve guess x) 
    (average guess (/ x guess)))

(define (average x y) 
    (/ (+ y x) 2 ))

(define (good-enough? guess next-guess)
    (< (abs (- next-guess guess) ) (* guess 0.0001)))

(define (sqrt x) (sqrt-iter 1.0 x))

(square (sqrt 1000))

(square (sqrt 1.0))

(square (sqrt 0.5))
(square (sqrt 0.0004))

(square 1234)
(sqrt 1522756)
(sqrt (square 1234))