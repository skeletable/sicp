(define (square x) (* x x))

(define (bigsquare a b c) 
    (cond ((and (> a c) (> b c)) (+ (square a) (square b)))
          ((and (> a b) (> c b)) (+ (square a) (square c)))
          (else (+ (square b) (square c)))))

(bigsquare 1 2 3)
(bigsquare 3 1 2)
(bigsquare 2 3 1)