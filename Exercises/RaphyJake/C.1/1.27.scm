
(define (fool-test n)

  (define (square x) (* x x))

  (define (even? x) (= 0 (remainder x 2)))

  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
          (else (remainder (* base (expmod base (- exp 1) m)) m))))

  (define (iter-test n a)
    (cond ((= n a) #t)
          ((= a (expmod a n n)) (iter-test n (+ 1 a)))
          (else #f)))

  (iter-test n 1))


(fool-test 19)
(fool-test 561) ; divisible by 3
(fool-test 1105)
(fool-test 1729)
(fool-test 80) ; not prime
