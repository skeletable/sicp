(define (sum term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

(define (naturals b)
  (define (id x) x)
  (define (inc x) (+ 1 x))

  (sum id 1 inc b)
  )

(naturals 10)
