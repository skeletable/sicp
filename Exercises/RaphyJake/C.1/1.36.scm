
(define (fix f x tolerance)
  (define (close-enough? p1 p2) (< (abs (- p1 p2)) tolerance))

  (define (try guess)
    (newline)
    (display guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try x))


(fix (lambda (x) (/ (log 1000) (log x))) 2.0 0.0001)

(newline)

(define (average x y) (/ (+ x y) 2))

(fix (lambda (x) (average x (/ (log 1000) (log x)))) 2.0 0.0001)
