```
(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))
```
If `b` is bigger than `0`, the function will return the sum of `a` and `b`, otherwise it will subtract `b` from `a`