(define (square x) (* x x))
(define (sqsum a b c)
  (cond ((and (> a c) (> b c)) (+ (square a) (square b)))
		((and (> b a) (> c a)) (+ (square b) (square c)))
		(else (+ (square a) (square c)))))

(print (sqsum 1 2 3))
(print (sqsum 3 2 1))
(print (sqsum 2 4 6))
