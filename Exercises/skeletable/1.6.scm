(define (new-if predicate 
                then-clause 
                else-clause)
  (cond (predicate then-clause)
        (else else-clause)))

(print (new-if (= 2 3) 0 5))
(print (new-if (= 1 1) 0 5))

(define (sqrt-iter guess x)
  (new-if (good-enough? guess x) ;obviously this won't work
          guess
          (sqrt-iter (improve guess x) x)))

(print (sqrt-iter 1 2))
